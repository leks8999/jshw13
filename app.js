/* Ответы на теоретические вопросы:
1. setTimeout вызывает функцию один раз через заданное время, а setInterval вызывает функцию постоянно через заданный 
интервал времени.
2. Если в setTimeout() передать нулевую задержку, то вызов функции будет запланирован сразу после выполнения текущего кода,
потому как ф-ция помещаеться в планировщик через 0мс, но он проверит список только после завершения текущего кода.
3. clearInterval() нужен для остановки setInterval() иначе он будет выполняться безконечно.*/

const img = document.querySelectorAll('img')
const firstImg = img[0];
const lastImg = img[img.length - 1];
const allBtn = document.querySelector('.btn-wrapper')
const stop = document.querySelector('.stop')
const start = document.querySelector('.start')

const slider = () => {
    const currentImg = document.querySelector('.active');
    if (currentImg !== lastImg) {
        currentImg.classList.remove('active');
        currentImg.nextElementSibling.classList.add('active');
    } else {
        currentImg.classList.remove('active')
        firstImg.classList.add('active');
    }
}

let timer = setInterval(slider, 3000);

allBtn.addEventListener('click', (e) => {
    if (e.target === stop) {
        clearInterval(timer)
    } else if (e.target === start) {
        timer = setInterval(slider, 3000);
    }
})


